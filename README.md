# apk-snap

![Tests](https://github.com/wesbarnett/snap-pac/workflows/Tests/badge.svg)
![Docs](https://github.com/wesbarnett/snap-pac/workflows/Docs/badge.svg)

## Synopsis

`apk-snap` is a fork of Wes Barnetts [snap-pac](https://github.com/wesbarnett/snap-pac), originally made for Arch Linux and `pacman`.
`apk-snap` ports the tool to Alpine Linux and `apk`.

This is a set of [apk](https://wiki.alpinelinux.org/wiki/Alpine_Package_Keeper) hooks and script
that automatically causes [snapper](http://snapper.io/) to perform a pre and post
snapshot before and after apk transactions, similar to how YaST does with OpenSuse.
This provides a simple way to undo changes to a system after an apk transaction.

For more information, [see the documentation](https://wesbarnett.github.io/snap-pac/).

## Installation

Install the `apk-snap` package using apk.

For instructions on how to install without apk, [see the documentation](https://wesbarnett.github.io/snap-pac/installation.html).

## Configuration

Most likely, configuration is not needed. By default, the snapper configuration named
`root` will have pre/post snapshots taken for every apk transaction.

For more information on configuring apk-snap, see [the documentation](https://wesbarnett.github.io/snap-pac/configuration.html).

## Documentation

See the [documentation here](https://wesbarnett.github.io/snap-pac/) or `man 8 apk-snap`
after installation.

## Troubleshooting

After reviewing the documentation, [check the issues page] and file a new issue if your
problem is not covered.

[check the issues page]: https://gitlab.com/papiris/snap-apk/-/issues
