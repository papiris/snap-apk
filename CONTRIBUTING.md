If you like apk-snap, please consider donating to me on [Liberapay](https://liberapay.com/papiris/)

Besides donations; you can contribute by filing issues, improving code and documentation :)
