FAQ
===

.. toctree::
   :maxdepth: 2

**Does apk-snap backup non-btrfs /boot partitions?**

No, but you can add a hook that does it for you. It would be something like the following:

.. code-block:: none

	#!/bin/sh
	if [ "$1" = "pre-commit" ]; then
		/usr/bin/rsync -avzq --delete /boot /.bootbackup
	fi

Note that you will probably want to name the file with a numbered prefix less than
``05`` so that it is run before the apk-snap pre snapshot takes place. That will ensure
that the snapshot taken will have the boot partition back-up corresponding with the
state of the system. For example, you could name it ``04-backupboot.hook``.
