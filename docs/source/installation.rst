Installation
============

Install the ``apk-snap`` package using pacman:

.. code-block:: ash

   apk add apk-snap

Alternatively download the latest release or tag
<https://gitlab.com/papiris/apk-snap/-/tags>`_.



Finally, run:

.. code-block:: ash

    make install



Dependencies
------------

``python``, ``apk``, and ``snapper`` are all required.

Testing
-------

For testing, ``pytest`` is required. To run the tests do:

.. code-block:: ash

   make test


Documentation
-------------

Typically, you will not need to build the documentation on your own and can simply
access it by visiting the `online documentation
<https://wesbarnett.github.io/snap-pac/>`_ or by accessing the manpage:

.. code-block:: ash

   man 8 apk-snap

To build the documentation, ``sphinx`` is required. To build the documentation you can
do:

.. code-block:: ash

   make docs

The resulting html documentation will then be located at ``docs/build/index.html``.
Additionally, this generates the manpage which will be located under ``man8``.
