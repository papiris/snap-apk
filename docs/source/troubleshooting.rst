Troubleshooting
===============

.. toctree::
   :maxdepth: 2

**apk-snap is only taking snapshots of the root configuration.**

That's the default behavior. See :doc:`configuration`.

**No snapshots are being taken when I run apk.**

No snapper configurations are set up for apk-snap's apk hook. By default apk-snap
will take snapshots for the root configuration and any other configuration which has
SNAPSHOT set to yes in its configuration file. See :doc:`configuration`.

