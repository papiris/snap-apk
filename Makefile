# apk-snap
# https://gitlab.com/papiris/snap-apk
# Copyright (C) 2016-2021 James W. Barnett
# Copyright (C) 2023 Jacob D. Ludvigsen

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

PKGNAME = apk-snap
PREFIX ?= /usr

SHARE_DIR = $(DESTDIR)$(PREFIX)/share

.PHONY: install test docs

install:
	@install -Dm755 scripts/apk_snap.py $(DESTDIR)$(PREFIX)/bin/apk-snap
	@install -Dm644 LICENSE -t $(SHARE_DIR)/licenses/$(PKGNAME)/
	@install -Dm644 man8/* -t $(SHARE_DIR)/man/man8/
	@install -Dm644 README.md -t $(SHARE_DIR)/doc/$(PKGNAME)/
	@install -Dm644 extra/apk-snap.ini $(DESTDIR)/etc/apk-snap/apk-snap.ini
	@install -Dm755 hooks/* -t $(DESTDIR)/etc/apk/commit_hooks.d/

test:
	@python -m pytest -v .

man:
	@cd docs && make man
	@mkdir -p man8
	@awk 'NR==33{print ".SH DESCRIPTION"}7' docs/build/man/apk-snap.8 > man8/apk-snap.8

docs: man
	@sphinx-build -a docs/source docs/build
